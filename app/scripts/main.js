'use strict';

const main = () => {
  const navbarMenu = document.querySelector('nav ul');
  const navbarMenuButton = document.getElementById('navigation-menu-button');
  const navbar = document.querySelector('[role=navigation]');

  function toggleNavigationMenuButtonState() {
    if (navbarMenuButton.innerHTML === 'Menu') {
      navbarMenuButton.innerHTML = 'Close';
    } else {
      navbarMenuButton.innerHTML = 'Menu';
    }
  }

  function toggleMobileNavbarState() {
    navbar.classList.toggle('mobile-expanded');
  }

  function toggleNavbarMenu() {
    navbarMenu.classList.toggle('open');
  }

  navbarMenuButton.addEventListener('click', function () {
    toggleNavbarMenu();
    toggleNavigationMenuButtonState();
    toggleMobileNavbarState();
    document.body.classList.toggle('scroll-lock-for-mobile-nav');
  });

};

window.addEventListener('DOMContentLoaded', (event) => {
  console.log('DOM fully loaded and parsed... Calling main function');
  main();
});

'use strict';

const formValidationMain = () => {
  const allValidatingFormInputs = document.querySelectorAll(':required');
  for (let i = 0; i < allValidatingFormInputs.length; i++) {
    const allValidatingFormInput = allValidatingFormInputs[i]
    if(!allValidatingFormInput.validity.valid)
    {
      allValidatingFormInput.focus();
      // break
      return false;
    }
  }

};

window.addEventListener('DOMContentLoaded', (event) => {
  formValidationMain();
});
